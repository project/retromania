<?php
?>
<li class="widget block-<?php print $block->module ?>" id="block-<?php print $block->module . '-' . $block->delta; ?>">
   <?php if ($block->subject): ?><h2 class="widgettitle"><?php print $block->subject ?></h2><?php endif; ?>	
   <?php print $block->content ?>
</li>