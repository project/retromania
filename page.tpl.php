<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
 
</head>
<body>

  <div class="wrap">
    <div class="header">
      <h1 class="logo"><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
      <?php if ($site_slogan): ?><div class="description1"><?php print $site_slogan; ?></div><?php endif; ?>
      <?php print $feed_icons; ?>
      <?php print $p_links; ?>
    </div>
    <div class="content">
      <div class="content_left">
        <div class="content_right"><!-- Mainbar -->
          <div class="mainbar">
            <div class="mainbar_top">
              <div class="mainbar_bottom">
                <div class="mainbar_inner">
                  <?php if ($is_front && !empty($mission)): ?>
                     <div >
                       <p><strong><?php print $mission; ?></strong></p>
                     </div>
                  <?php endif; ?>
                  <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   	   			  <?php if (!empty($title) && !$node): ?><h2 ><?php print $title; ?></h2><?php endif; ?>
       			  <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
   	  			  <?php if (!empty($messages)): print $messages; endif; ?>
      			  <?php if (!empty($help)): print $help; endif; ?> 
                  <?php print $content; ?>
                </div>
              </div>
            </div>
          </div>  
  <!-- Sidebar -->  
 
        <div class="sidebar">
	      <ul>
	        <?php if ($search_box): ?>
              <li>
                <?php print $search_box ?>
                            
              </li>
    	    <?php endif; ?>
    	    
    	    <?php print $right ?>
          </ul>
        </div>  
        <div class="clear"></div>
      </div>
    </div>
			<div class="footer">
            	<div class="footer_left">
            	<div class="footer_right">
                	<div class="copy"><?php print $footer_msg; ?></div>
                    <div class="copy_support">Theme created by <a href="http://www.jayhafling.com/">Jay Hafling</a> and top <a href="http://topdrupalthemes.net/">drupal themes</a>.</div>
            	</div>
            	</div>
            </div>
        </div>
    </div>
    <?php print $closure; ?>
    </body>
</html>