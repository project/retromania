<?php

function retromania_preprocess_page(&$vars) {
  $vars['footer_msg'] = ' &copy; ' . $vars['site_name'] . ' ' . date('Y');
  $vars['search_box'] = str_replace(t('Search this site: '), '', $vars['search_box']);

  $vars['p_links'] = '';
  if(!empty($vars['primary_links'])) {

    foreach ($vars['primary_links'] as $link) {
      $link_current = '';
      $attributes = 'class="page_item"';
      $href_attributes = '';
      $href = url($link['href']);
      $vars['p_links'] .= '<li ' . $attributes . '><a ' . $href_attributes . ' href="' . $href . '" >' . $link['title'] . '</a></li>';
    }
    $vars['p_links'] = '<ul class="menu1" id="nav">' . $vars['p_links'] . '</ul>';
  }

}

function retromania_preprocess_node(&$vars) {
  $vars['post_day'] = format_date($vars['node']->created, 'custom', 'd');
  $vars['post_month'] = format_date($vars['node']->created, 'custom', 'M');
  $vars['author'] = theme('username', $vars['node']);
  if ($vars['author'])
  $vars['posted_by'] = t('By') . ' ' . $vars['author'];
}

function retromania_preprocess_comment_wrapper(&$vars) {
  $node = $vars['node'];
  $vars['header'] = t('<strong>!count comments</strong> on %title', array('!count' => $node->comment_count, '%title' => $node->title));
}

function retromania_preprocess_comment(&$vars) {
  $vars['classes'] = array('comment');
  if ($vars['zebra'] == 'even') {
    $vars['classes'][] = 'alt';
  }
  $vars['classes'] = implode(' ', $vars['classes']);
}

function retromania_fix_tags($terms, $separator = ' ') {
  $output = '';
  if (is_array($terms)) {
    $links = array();

    foreach ($terms as $term) {
      $links[] = l($term->name, taxonomy_term_path($term), array('rel' => 'tag', 'title' => strip_tags($term->description)));
    }

    $output .= implode($separator, $links);
  }

  return $output;
}

function retromania_fix_post_links($links, $separator = ' ') {
  $r = array();
  foreach($links as $link) {
    $r[] = '<a href="' . url($link['href']) . (!empty($link['fragment']) ? '#' . $link['fragment'] : '') . '" ' . retromania_link_attributes($link['attributes']) . ' >' . $link['title'] . '</a>';
  }

  $r = implode($separator, $r);
  return $r;
}

function retromania_link_attributes($attributes) {
  $r = ' ';
  if (is_array($attributes)) {
    foreach ($attributes as $key => $value) {
      $r .= $key . '="' . $value . '" ';
    }
  }
  return $r;
}

function retromania_search_theme_form($form) {

  $form['#id'] = 'searchform';
  $form['submit']['#type'] = 'button';
  $form['submit']['#attributes']['class'] = 'submit_search';
  $form['submit']['#value'] = t('Search');
  $form['search_theme_form']['#attributes']['class'] = 'search_input';
  $form['search_theme_form']['#id'] = 's';
  $form['search_theme_form']['#title'] = t('');
  return drupal_render($form);
}

function retromania_feed_icon($url, $title) {
  return '<div class="subscribe_email"><a href="' . check_url($url) . '" title="' . $title . '" rel="nofollow">' . $title . '</a></div>';
}



